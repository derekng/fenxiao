﻿namespace Hishop.Weixin.MP.Util
{
    using Hishop.Weixin.MP.Domain;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml.Linq;

    public static class EntityHelper
    {
        public static XDocument ConvertEntityToXml<T>(T entity) where T: class, new()
        {
            if (entity == null)
            {
            }
            entity = Activator.CreateInstance<T>();
            XDocument document = new XDocument();
            document.Add(new XElement("xml"));
            XElement root = document.Root;
            List<string> list = new string[] { "ToUserName", "FromUserName", "CreateTime", "MsgType", "Content", "ArticleCount", "Articles", "FuncFlag", "Title ", "Description ", "PicUrl", "Url" }.ToList<string>();
            Func<string, int> orderByPropName = new Func<string, int>(list.IndexOf);
            foreach (PropertyInfo info in (from p in entity.GetType().GetProperties()
                orderby orderByPropName(p.Name)
                select p).ToList<PropertyInfo>())
            {
                DateTime time;
                string name = info.Name;
                if (name == "Articles")
                {
                    XElement content = new XElement("Articles");
                    List<Article> list3 = info.GetValue(entity, null) as List<Article>;
                    foreach (Article article in list3)
                    {
                        IEnumerable<XElement> enumerable = ConvertEntityToXml<Article>(article).Root.Elements();
                        content.Add(new XElement("item", enumerable));
                    }
                    root.Add(content);
                }
                else
                {
                    string str2 = info.PropertyType.Name;
                    if (str2 == null)
                    {
                        goto Label_0328;
                    }
                    if (!(str2 == "String"))
                    {
                        if (str2 == "DateTime")
                        {
                            goto Label_0252;
                        }
                        if (str2 == "Boolean")
                        {
                            goto Label_028A;
                        }
                        if (str2 == "ResponseMsgType")
                        {
                            goto Label_02D0;
                        }
                        if (str2 == "Article")
                        {
                            goto Label_02FC;
                        }
                        goto Label_0328;
                    }
                    root.Add(new XElement(name, new XCData((info.GetValue(entity, null) as string) ?? "")));
                }
                continue;
            Label_0252:
                time = (DateTime) info.GetValue(entity, null);
                root.Add(new XElement(name, time.Ticks));
                continue;
            Label_028A:
                if (!(name == "FuncFlag"))
                {
                    goto Label_0328;
                }
                root.Add(new XElement(name, ((bool) info.GetValue(entity, null)) ? "1" : "0"));
                continue;
            Label_02D0:
                root.Add(new XElement(name, info.GetValue(entity, null).ToString().ToLower()));
                continue;
            Label_02FC:
                root.Add(new XElement(name, info.GetValue(entity, null).ToString().ToLower()));
                continue;
            Label_0328:
                root.Add(new XElement(name, info.GetValue(entity, null)));
            }
            return document;
        }

        public static void FillEntityWithXml<T>(T entity, XDocument doc) where T: AbstractRequest, new()
        {
            if (entity == null)
            {
            }
            entity = Activator.CreateInstance<T>();
            XElement root = doc.Root;
            foreach (PropertyInfo info in entity.GetType().GetProperties())
            {
                string name = info.Name;
                if (root.Element(name) != null)
                {
                    switch (info.PropertyType.Name)
                    {
                        case "DateTime":
                        {
                            info.SetValue(entity, new DateTime(long.Parse(root.Element(name).Value)), null);
                            continue;
                        }
                        case "Boolean":
                        {
                            if (!(name == "FuncFlag"))
                            {
                                break;
                            }
                            info.SetValue(entity, root.Element(name).Value == "1", null);
                            continue;
                        }
                        case "Int64":
                        {
                            info.SetValue(entity, long.Parse(root.Element(name).Value), null);
                            continue;
                        }
                        case "Int32":
                        {
                            info.SetValue(entity, int.Parse(root.Element(name).Value), null);
                            continue;
                        }
                        case "RequestEventType":
                        {
                            info.SetValue(entity, EventTypeHelper.GetEventType(root.Element(name).Value), null);
                            continue;
                        }
                        case "RequestMsgType":
                        {
                            info.SetValue(entity, MsgTypeHelper.GetMsgType(root.Element(name).Value), null);
                            continue;
                        }
                    }
                    info.SetValue(entity, root.Element(name).Value, null);
                }
            }
        }
    }
}

