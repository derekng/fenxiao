﻿namespace Hidistro.UI.Web.Admin.Settings
{
    using Hidistro.ControlPanel.WeiXin;
    using Hidistro.Core;
    using Hidistro.Core.Entities;
    using Hidistro.UI.Common.Controls;
    using Hidistro.UI.ControlPanel.Utility;
    using System;
    using System.Web.UI.HtmlControls;
    using System.Web.UI.WebControls;

    public class WeixinPay : AdminPage
    {
        protected bool _enable;
        protected Script Script4;
        private SiteSettings siteSettings;
        protected HtmlForm thisForm;
        protected TextBox txt_appid;
        protected TextBox txt_appsecret;
        protected TextBox txt_key;
        protected TextBox txt_mch_id;

        protected WeixinPay() : base("m06", "wxp08")
        {
            this.siteSettings = SettingsManager.GetMasterSettings(false);
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!base.IsPostBack)
            {
                this.txt_appid.Text = this.siteSettings.WeixinAppId;
                this.txt_appsecret.Text = this.siteSettings.WeixinAppSecret;
                this.txt_key.Text = this.siteSettings.WeixinPartnerKey;
                this.txt_mch_id.Text = this.siteSettings.WeixinPartnerID;
            }
            this._enable = this.siteSettings.EnableWeiXinRequest;
        }

        private void saveData()
        {
            if (string.IsNullOrEmpty(this.txt_appid.Text.Trim()))
            {
                this.ShowMsg("请输入appid！", false);
            }
            if (string.IsNullOrEmpty(this.txt_appsecret.Text.Trim()))
            {
                this.ShowMsg("请输入appsecret！", false);
            }
            if (string.IsNullOrEmpty(this.txt_key.Text.Trim()))
            {
                this.ShowMsg("请输入Key！", false);
            }
            if (string.IsNullOrEmpty(this.txt_mch_id.Text.Trim()))
            {
                this.ShowMsg("请输入mch_id！", false);
            }
            if (this.siteSettings.WeixinAppId != this.txt_appid.Text.Trim())
            {
                WeiXinHelper.ClearWeiXinMediaID();
            }
            this.siteSettings.WeixinAppId = this.txt_appid.Text.Trim();
            this.siteSettings.WeixinAppSecret = this.txt_appsecret.Text.Trim();
            this.siteSettings.WeixinPartnerKey = this.txt_key.Text.Trim();
            this.siteSettings.WeixinPartnerID = this.txt_mch_id.Text.Trim();
            SettingsManager.Save(this.siteSettings);
            this.ShowMsg("保存成功！", true);
        }

        protected void Unnamed_Click(object sender, EventArgs e)
        {
            this.saveData();
        }
    }
}

