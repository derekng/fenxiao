﻿namespace Hidistro.UI.Web.Admin.promotion
{
    using Hidistro.ControlPanel.Members;
    using Hidistro.Entities.Members;
    using Newtonsoft.Json;
    using System;
    using System.Collections.Generic;
    using System.Text;
    using System.Web;

    public class GetMemberGradesHandler : IHttpHandler
    {
        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "text/plain";
            try
            {
                new StringBuilder();
                IList<MemberGradeInfo> memberGrades = MemberHelper.GetMemberGrades();
                List<SimpleGradeClass> list2 = new List<SimpleGradeClass>();
                if (memberGrades.Count > 0)
                {
                    foreach (MemberGradeInfo info in memberGrades)
                    {
                        SimpleGradeClass item = new SimpleGradeClass {
                            GradeId = info.GradeId,
                            Name = info.Name
                        };
                        list2.Add(item);
                    }
                }
                var type = new {
                    type = "success",
                    data = list2
                };
                string s = JsonConvert.SerializeObject(type);
                context.Response.Write(s);
            }
            catch (Exception exception)
            {
                context.Response.Write("{\"type\":\"error\",data:\"" + exception.Message + "\"}");
            }
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}

