﻿namespace Hidistro.UI.Web.Pay
{
    using Hidistro.Core;
    using Hidistro.Core.Entities;
    using Hidistro.Entities.Orders;
    using Hidistro.SaleSystem.Vshop;
    using Hishop.Weixin.Pay;
    using Hishop.Weixin.Pay.Notify;
    using System;
    using System.Collections.Generic;
    using System.Web.UI;

    public class wx_Pay : Page
    {
        protected string OrderId;
        protected List<OrderInfo> orderlist;

        protected void Page_Load(object sender, EventArgs e)
        {
            SiteSettings masterSettings = SettingsManager.GetMasterSettings(false);
            PayNotify payNotify = new NotifyClient(masterSettings.WeixinAppId, masterSettings.WeixinAppSecret, masterSettings.WeixinPartnerID, masterSettings.WeixinPartnerKey, masterSettings.WeixinPaySignKey).GetPayNotify(base.Request.InputStream);
            if (payNotify != null)
            {
                this.OrderId = payNotify.PayInfo.OutTradeNo;
                this.orderlist = ShoppingProcessor.GetOrderMarkingOrderInfo(this.OrderId);
                if (this.orderlist.Count == 0)
                {
                    base.Response.Write("success");
                }
                else
                {
                    foreach (OrderInfo info in this.orderlist)
                    {
                        info.GatewayOrderId = payNotify.PayInfo.TransactionId;
                    }
                    this.UserPayOrder();
                }
            }
        }

        private void UserPayOrder()
        {
            foreach (OrderInfo info in this.orderlist)
            {
                if (info.OrderStatus == OrderStatus.BuyerAlreadyPaid)
                {
                    base.Response.Write("success");
                    return;
                }
            }
            foreach (OrderInfo info2 in this.orderlist)
            {
                if (info2.CheckAction(OrderActions.BUYER_PAY) && MemberProcessor.UserPayOrder(info2))
                {
                    info2.OnPayment();
                    base.Response.Write("success");
                }
            }
        }
    }
}

