﻿namespace Hidistro.UI.Web.API
{
    using Hidistro.Core;
    using Hidistro.Core.Entities;
    using System;
    using System.Web;

    public class Hi_Ajax_OnlineServiceConfig : IHttpHandler
    {
        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "text/plain";
            if (!SettingsManager.GetMasterSettings(false).EnableSaleService)
            {
                context.Response.Write("");
            }
            else
            {
                CustomerServiceSettings masterSettings = CustomerServiceManager.GetMasterSettings(false);
                context.Response.Write(string.Format("<script src='//meiqia.com/js/mechat.js?unitid={0}' charset='UTF-8' async='async'></script>", masterSettings.unitid));
            }
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}

