﻿namespace Hidistro.UI.Web.API
{
    using Hidistro.ControlPanel.Settings;
    using Hidistro.Core;
    using Hidistro.Core.Entities;
    using Hidistro.Entities.Settings;
    using Hidistro.SaleSystem.Vshop;
    using Newtonsoft.Json;
    using System;
    using System.Collections.Generic;
    using System.Web;

    public class Hi_Ajax_NavMenu : IHttpHandler
    {
        public IList<MenuInfo> GetAllMenu()
        {
            IList<MenuInfo> list = new List<MenuInfo>();
            return MenuHelper.GetMenus();
        }

        public string GetPhone()
        {
            int currentDistributorId = Globals.GetCurrentDistributorId();
            if (currentDistributorId == 0)
            {
                return SettingsManager.GetMasterSettings(true).ShopTel;
            }
            return MemberProcessor.GetMember(currentDistributorId, true).CellPhone;
        }

        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "text/plain";
            IList<MenuInfo> allMenu = this.GetAllMenu();
            SiteSettings masterSettings = SettingsManager.GetMasterSettings(false);
            context.Response.Write(JsonConvert.SerializeObject(new { status = 1, msg = "", Phone = this.GetPhone(), GuidePage = SettingsManager.GetMasterSettings(true).GuidePageSet, ShopDefault = masterSettings.ShopDefault, MemberDefault = masterSettings.MemberDefault, GoodsType = masterSettings.GoodsType, GoodsCheck = masterSettings.GoodsCheck, ActivityMenu = masterSettings.ActivityMenu, DistributorsMenu = masterSettings.DistributorsMenu, GoodsListMenu = masterSettings.GoodsListMenu, BrandMenu = masterSettings.BrandMenu, ShopMenuStyle = masterSettings.ShopMenuStyle, menuList = allMenu }));
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}

