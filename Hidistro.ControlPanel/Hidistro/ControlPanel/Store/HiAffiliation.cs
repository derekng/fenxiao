﻿namespace Hidistro.ControlPanel.Store
{
    using Hidistro.Entities.Members;
    using Hidistro.SaleSystem.Vshop;
    using System;
    using System.Runtime.InteropServices;
    using System.Web;
    using System.Linq;

    public static class HiAffiliation
    {
        public static void ClearReferralIdCookie()
        {
            HttpCookie cookie = HttpContext.Current.Request.Cookies["Vshop-ReferralId"];
            if (((cookie != null) && !string.IsNullOrEmpty(cookie.Value)) && (int.Parse(cookie.Value) == 0))
            {
                cookie.Value = null;
                cookie.Expires = DateTime.Now.AddYears(-1);
                HttpContext.Current.Response.Cookies.Set(cookie);
            }
        }

        public static void ClearUserCookie(string url = "", bool isRedirect = false)
        {
            HttpCookie cookie = HttpContext.Current.Request.Cookies["Vshop-Member"];
            if ((cookie != null) && !string.IsNullOrEmpty(cookie.Value))
            {
                cookie.Value = null;
                cookie.Expires = DateTime.Now.AddYears(-1);
                HttpContext.Current.Response.Cookies.Set(cookie);
                if (isRedirect && !string.IsNullOrEmpty(url))
                {
                    HttpContext.Current.Response.Redirect(url);
                }
            }
        }

        public static string GetReturnUrl(string returnUrl)
        {
            if (returnUrl.IndexOf("?") > -1)
            {
                returnUrl = returnUrl.Substring(returnUrl.IndexOf("?"));
            }
            return returnUrl;
        }

        public static void LoadPage()
        {
            string str = ReturnUrl();
            if (!string.IsNullOrEmpty(str))
            {
                HttpContext.Current.Response.Redirect(str);
            }
            HttpCookie cookie = HttpContext.Current.Request.Cookies["Vshop-ReferralId"];
            if (((cookie != null) && !string.IsNullOrEmpty(cookie.Value)) && ((int.Parse(cookie.Value) != 0) && (DistributorsBrower.GetCurrentDistributors(Convert.ToInt16(cookie.Value)) == null)))
            {
                ClearReferralIdCookie();
                ClearUserCookie("/UserLogin.aspx", true);
            }
        }

        public static string ReturnUrl()
        {
            MemberInfo currentMember = MemberProcessor.GetCurrentMember();
            if (currentMember != null)
            {
                return ReturnUrlByUser(currentMember);
            }
            return ReturnUrlByQueryString();
        }

        public static string ReturnUrlByQueryString()
        {
            int result = 0;
            string str = HttpContext.Current.Request.Url.PathAndQuery.ToString();
            if (!HttpContext.Current.Request.QueryString.AllKeys.Contains("returnUrl"))
            {
                if (HttpContext.Current.Request.Url.AbsolutePath == "/logout.aspx")
                {
                    return string.Empty;
                }
                if (!string.IsNullOrEmpty(HttpContext.Current.Request.QueryString["ReferralId"]))
                {
                    if (int.TryParse(HttpContext.Current.Request.QueryString["ReferralId"], out result) && (result != 0))
                    {
                        HttpCookie cookie = HttpContext.Current.Request.Cookies["Vshop-ReferralId"];
                        if ((cookie != null) && (cookie.Value != result.ToString()))
                        {
                            SetReferralIdCookie(result.ToString(), "", false);
                        }
                    }
                }
                else if (HttpContext.Current.Request.Url.AbsolutePath != "/Default.aspx")
                {
                    HttpCookie cookie2 = HttpContext.Current.Request.Cookies["Vshop-ReferralId"];
                    if ((cookie2 != null) && !string.IsNullOrEmpty(cookie2.Value))
                    {
                        if (HttpContext.Current.Request.QueryString.Count > 0)
                        {
                            if (!HttpContext.Current.Request.QueryString.AllKeys.Contains("ReferralId"))
                            {
                                return (str + "&ReferralId=" + cookie2.Value);
                            }
                            return string.Empty;
                        }
                        if (!HttpContext.Current.Request.QueryString.AllKeys.Contains("ReferralId"))
                        {
                            return (str + "?ReferralId=" + cookie2.Value);
                        }
                        return string.Empty;
                    }
                }
                else
                {
                    SetReferralIdCookie("0", "", false);
                }
                if (!HttpContext.Current.Request.QueryString.AllKeys.Contains("returnUrl") && (HttpContext.Current.Request.Url.AbsolutePath != "/logout.aspx"))
                {
                    if (!HttpContext.Current.Request.QueryString.AllKeys.Contains("ReferralId") && (HttpContext.Current.Request.QueryString.Count > 0))
                    {
                        return (str + "&ReferralId=" + result.ToString());
                    }
                    if (!HttpContext.Current.Request.QueryString.AllKeys.Contains("ReferralId"))
                    {
                        return (str + "?ReferralId=" + result.ToString());
                    }
                }
            }
            return string.Empty;
        }

        public static string ReturnUrlByUser(MemberInfo mInfo)
        {
            string str = HttpContext.Current.Request.Url.PathAndQuery.ToString();
            DistributorsInfo currentDistributors = DistributorsBrower.GetCurrentDistributors(Convert.ToInt32(mInfo.UserId));
            if (currentDistributors != null)
            {
                SetReferralIdCookie(currentDistributors.UserId.ToString(), "", false);
            }
            else
            {
                SetReferralIdCookie(mInfo.ReferralUserId.ToString(), "", false);
            }
            HttpCookie cookie = HttpContext.Current.Request.Cookies["Vshop-ReferralId"];
            if ((cookie != null) && !string.IsNullOrEmpty(cookie.Value))
            {
                if (!string.IsNullOrEmpty(HttpContext.Current.Request.QueryString["ReferralId"]))
                {
                    HiUriHelp help = new HiUriHelp(HttpContext.Current.Request.Url.Query);
                    string queryString = help.GetQueryString("ReferralId");
                    if (!string.IsNullOrEmpty(queryString))
                    {
                        if (queryString == cookie.Value)
                        {
                            return string.Empty;
                        }
                        help.SetQueryString("ReferralId", cookie.Value);
                        return (HttpContext.Current.Request.Url.AbsolutePath + help.GetNewQuery());
                    }
                }
                if (!HttpContext.Current.Request.QueryString.AllKeys.Contains("returnUrl"))
                {
                    if (HttpContext.Current.Request.Url.AbsolutePath == "/logout.aspx")
                    {
                        return string.Empty;
                    }
                    if (!HttpContext.Current.Request.QueryString.AllKeys.Contains("ReferralId") && (HttpContext.Current.Request.QueryString.Count > 0))
                    {
                        return (str + "&ReferralId=" + cookie.Value);
                    }
                    if (!HttpContext.Current.Request.QueryString.AllKeys.Contains("ReferralId"))
                    {
                        return (str + "?ReferralId=" + cookie.Value);
                    }
                }
            }
            return string.Empty;
        }

        public static void SetReferralIdCookie(string referralId, string url = "", bool isRedirect = false)
        {
            ClearReferralIdCookie();
            HttpCookie cookie = HttpContext.Current.Request.Cookies["Vshop-ReferralId"];
            if (cookie == null)
            {
                cookie = new HttpCookie("Vshop-ReferralId");
            }
            cookie.Value = referralId;
            cookie.Expires = DateTime.Now.AddYears(1);
            HttpContext.Current.Response.Cookies.Set(cookie);
            if (isRedirect && !string.IsNullOrEmpty(url))
            {
                HttpContext.Current.Response.Redirect(url);
            }
        }

        public static void SetUserCookie(string userID)
        {
            ClearUserCookie("", false);
            HttpCookie cookie = HttpContext.Current.Request.Cookies["Vshop-Member"];
            if (cookie == null)
            {
                cookie = new HttpCookie("Vshop - Member");
            }
            cookie.Value = userID;
            cookie.Expires = DateTime.Now.AddYears(1);
            HttpContext.Current.Response.Cookies.Set(cookie);
        }
    }
}

